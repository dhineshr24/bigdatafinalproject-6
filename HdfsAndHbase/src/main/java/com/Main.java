package com;

import com.builder.BuilderClass;

public class Main {

    public static void main(String[] args) throws Exception {

        BuilderClass builderClass = new BuilderClass();
        builderClass.buildTask(args);

    }
}
